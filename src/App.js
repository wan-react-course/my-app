import './App.css';
import Cronometro from  './cronometro/Cronometro'
import CronometroF from './cronometro/CronometoF';
import { Route, Routes } from 'react-router-dom';
import { PaginaInicio } from './paginas/PaginaInicio/PaginaInicio';
import PaginaNoticias from './paginas/PaginaNoticias/PaginaNoticias';

function App() {
  return (
    <Routes>
      <Route path="/" element={<PaginaInicio />} exact/>
      <Route path="/cronometro/class" element={<Cronometro />} exact/>
      <Route path="/cronometro/funcion" element={<CronometroF />} exact/>
      <Route path="/noticias" element={<PaginaNoticias />} exact /> 
    </Routes>
  );
}

export default App;
