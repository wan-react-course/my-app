import React from "react";
import { Link } from "react-router-dom";

class ClassCronometro extends React.Component {
  constructor(props) {
    super(props);
    // This binding is necessary to make `this` work in the callback
    this.detener = this.detener.bind(this);
    this.limpiar = this.limpiar.bind(this);

    this.state = {
      tiempo: 0,
      estaCompleto: false,
    };
  }

  componentDidMount() {
    console.log('El component se monto en el DOM');
  }

  componentWillUnmount() {
    console.log('El component esta apunto de ser desmontadon del DOM');
    this.detener();
  }

  componentDidUpdate() {
    console.log('El componente se ha actualizado');
  }

  iniciar() {
    //evita correr n intervals al mismo tiempo
    //en caso de que interval exista se resetea el cronometro
    /**
     * subproceso1 = setInterval()
     * subproceso2 = setInterval()
     * subproceso3 = setInterval()
     */
    if(this.interval) {
      clearInterval(this.interval);
      this.interval = null;
      this.limpiar();
    }

    this.interval = setInterval(() => {
      // Incorrecto porque setState es una funcion asincrona
      //this.setState({
      //  tiempo : this.state.time + 1 
      //});

      // Correcto!!
      //const variable = 10;
      //this.setState({
      //  tiempo: variable
      //});

      const action = (estado) => ({
        tiempo: estado.tiempo + 1,
        estaCompleto: (estado.tiempo > 10) ? true : false
      });
      this.setState(action);
    }, 1000);
  }

  detener() {
    clearInterval(this.interval);
    this.interval = null;
  }

  limpiar() {
    this.setState({
      tiempo: 0
    });
  }



  render() {
    return (
      <div>
        <h1>{this.state.tiempo}</h1>
        <button onClick={() => this.iniciar()}>iniciar</button>
        <button onClick={this.detener}>detener</button>
        <button onClick={this.limpiar}>limpiar</button>

        <br />
        <Link to="/">ir a inicio</Link>
      </div>
    )
  }
}

export default ClassCronometro;