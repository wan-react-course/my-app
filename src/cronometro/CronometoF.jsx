import { useCallback, useEffect, useRef, useState } from "react"
import { Link } from "react-router-dom";

const CronometroF = () => {
  /**
   * hook en react
   */
  /**
   * useState()[0] 
   * useState()[1]
   */
  const intervalC = useRef(null);
  const esCompleto = useRef(false);
  const [tiempo, setTiempo] = useState(0);

  const iniciar = useCallback(() => {
    //evita correr n intervals al mismo tiempo
    //en caso de que interval exista se resetea el cronometro
    /**
     * subproceso1 = setInterval()
     * subproceso2 = setInterval()
     * subproceso3 = setInterval()
     */
     if(intervalC.current) {
      clearInterval(intervalC.current);
      intervalC.current = null;
      limpiar();
    }
    let nuevoTiempo = tiempo;
    intervalC.current = setInterval(() => {
      console.log(tiempo);
      nuevoTiempo += 1;
      setTiempo(nuevoTiempo);
      esCompleto.current = (nuevoTiempo > 10) ? true : false;
    }, 1000);
  }, [tiempo]);
  
  const detener = () => {
    clearInterval(intervalC.current);
  };

  const limpiar = () => {
    setTiempo(0);
  }

  /**
   * Este efecto representa al metodo del ciclo de vida componentDidMount 
   */
  useEffect(() => {
    console.log('el component cronometro funcional se monto');
  }, []);

  /**
   * Este efecto representa al metodo del ciclo de vida componentWillUnmount
   */
  useEffect(() => {
    return () => {
      console.log('El componente cronometro funcional se desmontara');
    }
  }, []);

  /**
   * Este efecto se ejectua la primera vez(cuando se monta el componente ) y posterior
   * cada que dependecia [tiempo] cambia.
   */
  useEffect(() => {
    console.log('la variable del estado(tiempo) ha cambiado');
  }, [tiempo]);


  console.log('Estoy renderiando funtion component');
  return (
    <div>
      <h1>{tiempo}</h1>
      <button onClick={iniciar}>iniciar</button>
      <button onClick={detener}>detener</button>
      <button onClick={limpiar}>limpiar</button>
      <br />
      <Link to="/">ir a inicio</Link>
    </div>
  );
}

export default CronometroF;