/**
 * 
 * @param {Object} props
 * @param {string} props.text
 * @param {string} props.className
 * @param {function(): void} props.onClick
 * @returns 
 */
const Button = (props) => {
  return (
    <button onClick={props.onClick} className={`custom-button ${props.className}`}>
      {props.text}
    </button>
  );
}

export default Button;