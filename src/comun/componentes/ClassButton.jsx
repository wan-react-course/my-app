import React from 'react';

class ClassButton extends React.Component { 

  constructor(props){
    super(props);
  }

  render() {
    return (
      <button onClick={this.props.onClick} className={`custom-button ${this.props.className}`}>
        {this.props.text}
      </button>
    );
  }
}

export default ClassButton;