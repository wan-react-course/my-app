import axios from "axios"

export class ServicioNoticias {

  constructor() {
    this.httpClient = axios.create({
      baseURL: 'https://newsapi.org',
      headers: {
        'Authorization': 'd5954ed83bfd45a2b595a087d8ef884f'
      }
    });
  }
  
  /**
   * Hace peticion http a news API y retorna los articulos princilaes en Mexico.
   * @returns {Promise<Array>}
   */
  obtenerNoticiasPrincipalesEnMexico() {
    return new Promise((resolve, reject) => {
      this.httpClient.get('/v2/top-headlines', {
        params: {
          country: 'mx'
        }
      }).then((response) => {
        resolve(response.data.articles);
      }).catch((error) => {
        reject(error)
      });
    });
  }

  /**
   * Hace peticion http a news API y retorna los articulos.
   * @param {string} query 
   * @returns {Promise<Array>}
   */
  async obtenerNoticias(query) {
    const { data } = await this.httpClient.get('/v2/everything', {
      params: {
        q : query,
        sortBy: 'publishedAt'
      }
    });

    return data.articles;
  }
}