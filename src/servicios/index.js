import React from "react";
import { ServicioNoticias } from "./ServicioNoticias";

const servicioNoticias = new ServicioNoticias();
export const ServicioNoticiasContext = React.createContext(servicioNoticias);