import { Link } from "react-router-dom"

export const PaginaInicio = () => {
  return (
    <ul>
      <li><Link to="/cronometro/class">navegar a cronometro con class component</Link></li>
      <li><Link to="/cronometro/funcion">navegar a cronometro con funtion component</Link></li>
      <li><Link to="/noticias">navegar a pagina de noticias</Link></li>
    </ul>
  )
}