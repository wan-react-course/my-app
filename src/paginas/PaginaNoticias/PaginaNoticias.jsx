import React, { useCallback, useContext, useEffect, useState } from "react";
import { Alert, Button, Card, Col, Container, Form, Navbar, Row } from "react-bootstrap";
import logo from '../../logo.svg';
import { ServicioNoticiasContext } from "../../servicios";

const PaginaNoticias = () => {
  const servicioNoticias = useContext(ServicioNoticiasContext);
  const [topico, setTopico] = useState('');
  const [noticias, setNoticias] = useState([]);
  const [mensaje, setMensaje] = useState('');
 
  const topicoEstaCambiando = useCallback((e) => {
    console.log(e.target.value);
    setTopico(e.target.value);
  }, []);

  const buscar = useCallback(async () => {
    setMensaje('');
    try {
      const noticiasAPI = await servicioNoticias.obtenerNoticias(topico);
      setNoticias(noticiasAPI);
      if(noticiasAPI.length < 1) {
        setMensaje('No se encontraron noticias');
      }
    } catch(error) {
      console.log(error);
      setMensaje('Ocurrio un error al consultar news API');
    }
  }, [topico, servicioNoticias]);

  const obtenerNoticiasPrincipales = useCallback(async () => {
    setMensaje('');
    try {
      const noticiasAPI = await servicioNoticias.obtenerNoticiasPrincipalesEnMexico();
      setNoticias(noticiasAPI);
    } catch {
       setMensaje('Ocurrio un error al consultar news API');
    }
  }, [servicioNoticias])

  useEffect(() => {
    obtenerNoticiasPrincipales();
  }, [obtenerNoticiasPrincipales])

  /**
   * metodo para mostrar logica condicional en react
   * para usarlo solo utilza la siguiente sintaxis en tu JSX.ELEMENT(HTML)
   * {mostrarAlerta()}
   * @returns 
   */
  const mostrarAlerta = () => {
    if(mensaje) {
      return <Alert variant="dark">error a mostrar</Alert>;
    }
    return undefined;
  }

  return (
    <>
      <Navbar bg="dark">
        <Container className="search-bar">
          <img src={logo} width="50" height="50"/>
          <Form.Control
            placeholder="Que noticias quieres ver?"
            className="search-input"
            value={topico}
            onChange={topicoEstaCambiando}
          />
          <Button
            variant="info"
            className="ms-2"
            onClick={buscar}
          >
            Buscar
          </Button>
        </Container>
      </Navbar>
      <Container>
        { mensaje &&  <Alert variant="dark">{mensaje}</Alert> }
        <Row>
          {noticias.map((noticia, index) => (
            <Col key={`noticia-${index}`}>
              <Card style={{ width: '18rem' }}>
                <Card.Img variant="top" src={noticia.urlToImage} />
                <Card.Body>
                  <Card.Title>{noticia.title}</Card.Title>
                  <Card.Text>
                    {noticia.description}
                  </Card.Text>
                  <a href={noticia.url} target="_blank">Ver noticia completa</a>
                </Card.Body>
              </Card>
            </Col>
          ))}
        </Row>
      </Container>
    </>
  );
}

export default PaginaNoticias;